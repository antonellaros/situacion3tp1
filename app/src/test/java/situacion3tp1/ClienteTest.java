package situacion3tp1;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClienteTest {

    @Test public void setNombreClienteTest() {
        Cliente propietario1=new Cliente();
        propietario1.setNombre("Antonella");

        String nombreActual = propietario1.getNombre();

        assertEquals("Antonella", nombreActual);
        
    }
    
}
