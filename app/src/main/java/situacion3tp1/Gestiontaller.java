package situacion3tp1;

public class Gestiontaller {

    private String reparaciones;
    private String repuestos;
    private float costomanoDeObra;

    public void setReparaciones(String reparaciones){
        this.reparaciones = reparaciones;
    }

    public String getReparaciones(){
        return reparaciones;
    }

    public void setRepuestos(String repuestos){
        this.repuestos = repuestos;
    }

    public String getRepuestos(){
        return repuestos;
    }

    public void setManoDeObra(float manoDeObra){
        this.costomanoDeObra = manoDeObra;
    }

    public float getManoDeObra(){
        return costomanoDeObra;
    }
    public String toString(){
        return "Reparacion efectuada: " + reparaciones + ", Repuesto utilizado: " + repuestos + ", Costo de la mano de obra: " + costomanoDeObra;
    }
}

