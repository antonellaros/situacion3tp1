package situacion3tp1;

public class Automovil {

    private String marca;
    private int modelo;
    private String matricula;
    private int kilometraje;
    private String motivo;


    public void setMarca(String marca){
        this.marca = marca;
    }

    public String getMarca(){
        return marca;
    }

    public void setModelo(int modelo){
        this.modelo = modelo;
    }

    public int getModelo(){
        return modelo;
    }

    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public String getMatricula(){
        return matricula;
    }
    
    public void setKilometraje(int kilometraje){
        this.kilometraje = kilometraje;
    }
    public int getKilometraje(){
        return kilometraje;
    }
    public void setMotivo( String motivo){
        this.motivo = motivo;
    }
    public String getMotivo(){
        return motivo;
    }
    public String toString(){
        return "Datos del Automovil: " + ", Marca: " + marca + ", Modelo: " + modelo + ", Matricula; " + matricula + ", Kilometraje: " + kilometraje + ", Motivo por el cual ingresa el vehiculo: " + motivo;
    }
}
