package situacion3tp1;

public class Cliente {

    private String nombre;
    private String apellido;
    private int documento;
    private String domicilio;
    private int telefono;

    //Metodo get: Obteniendo el valor de los atributos.
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public int getDocumento(){
        return documento;
    }
    public String getDomicilio(){
        return domicilio;
    }
    public int getTelefono(){
        return telefono;
    }

    //Metodo set: Establece valor
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setDocumento(int documento){
        this.documento = documento;
    }
    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    public void setTelefono(int telefono){
        this.telefono = telefono;
    }
    public String toString(){
        return "Nombre del propietario: " + nombre + ", " + apellido +", Documento:  " + documento + ", Domicilio: " + domicilio + ", Telefono:" + telefono;
    }

    
}
